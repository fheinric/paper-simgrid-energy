#!/bin/zsh

~/experiments/setup_cores.zsh 12
~/experiments/disable_turbomode.zsh
sudo cpupower -c all frequency-set -g userspace && sudo cpupower -c all frequency-set -d 2300MHz -u 2300MHz

cd ~/src/NPB3.3-MPI-Simgrid/
path=($(realpath ../simgrid/bin) $path)

for cores in {1,4,12,48,96,144}; do
echo "------ NO TURBO, numactl ------"
smpirun -wrapper "numactl --cpunodebind=1" -np $cores -platform platform_taurus.xml -hostfile ~/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 --log=smpi_bench.thres:critical --cfg=tracing:yes --cfg=smpi/comp-adjustment-file:/home/cheinrich/tmp/2016-08-11-smpi-mpi-ratio.csv --cfg=tracing/filename:/tmp/tracing --cfg=tracing/smpi:1 --cfg=tracing/smpi/computing:yes --cfg=smpi/trace-call-location:1 --cfg=smpi/privatize-global-variables:yes --cfg=plugin:Energy ./bin/lu.C.$cores  >| /home/cheinrich/experiments/2016-11-20--SimGrid-LU/$(hostname)_lu_C_${cores}_no_turbo_numactl.out 2>&1
echo "------ NO TURBO, NO numactl ------"
smpirun -np $cores -platform platform_taurus.xml -hostfile ~/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 --log=smpi_bench.thres:critical --cfg=tracing:yes --cfg=smpi/comp-adjustment-file:/home/cheinrich/tmp/2016-08-11-smpi-mpi-ratio.csv --cfg=tracing/filename:/tmp/tracing --cfg=tracing/smpi:1 --cfg=tracing/smpi/computing:yes --cfg=smpi/trace-call-location:1 --cfg=smpi/privatize-global-variables:yes --cfg=plugin:Energy ./bin/lu.C.$cores  >| /home/cheinrich/experiments/2016-11-20--SimGrid-LU/$(hostname)_lu_C_${cores}_no_turbo_no_numactl.out 2>&1

done
