#!/usr/bin/env zsh
export TAU_MAX_RECORDS=83886080
export TAU_TRACE=1
export TAU_THROTTLE=0
sudo cpupower -c all frequency-set -g userspace; sudo cpupower -c all frequency-set -d 2300MHz -u 2300MHz
cd /home/cheinrich/src/NPB3.3-MPI-TAU
mkdir /tmp/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1
mkdir /home/cheinrich/experiments/2016-08-10/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/
make clean && make LU NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-08-10/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-08-10/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.compilation.stderr
cd /tmp/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1
echo 'mpirun -x TAU_THROTTLE -x TAU_TRACE -x TRACEDIR -x TAU_MAX_RECORDS --report-bindings --timestamp-output -np 12  --mca plm_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-08-10/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI-TAU/bin/lu.C.12 > /tmp/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.stdout 2> /tmp/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-08-10/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-08-10/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-08-10/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-08-10/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.machinefile
echo -n "$(date '+%s')," >>| /home/cheinrich/experiments/2016-08-10/experiment_timings
mpirun -x TAU_THROTTLE -x TAU_TRACE -x TRACEDIR -x TAU_MAX_RECORDS --report-bindings --timestamp-output -np 12  --mca plm_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-08-10/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI-TAU/bin/lu.C.12 > /tmp/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.stdout 2> /tmp/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.stderr
echo -n "$(date '+%s')," >>| /home/cheinrich/experiments/2016-08-10/experiment_timings
echo 'LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1' >>| /home/cheinrich/experiments/2016-08-10/experiment_timings
cp /tmp/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1/experiment.* /home/cheinrich/experiments/2016-08-10/LU_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=on_RUNID=1
