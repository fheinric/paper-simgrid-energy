#!/bin/zsh

~/experiments/setup_cores.zsh 12
~/experiments/disable_turbomode.zsh
sudo cpupower -c all frequency-set -g userspace && sudo cpupower -c all frequency-set -d 2300MHz -u 2300MHz

cd ~/src/hpl-2.2/bin/SMPI/
path=($(realpath ../../../simgrid/bin) $path)

for cores in {12,8,4,1}; do
unlink HPL.dat
ln -s HPL.dat.${cores} HPL.dat
echo "------ NO TURBO, numactl ------"
smpirun -wrapper "numactl --cpunodebind=1" -np $cores -platform platform_taurus_hpl_loopback_bandwidth_is_ethernet.xml -hostfile ~/src/hpl-2.2/bin/Debian/hostnames-144 --log=smpi_bench.thres:critical --cfg=tracing:yes --cfg=tracing/filename:/tmp/tracing --cfg=tracing/smpi:1 --cfg=tracing/smpi/computing:yes --cfg=smpi/privatize-global-variables:yes --cfg=plugin:Energy ./xhpl  >| ~/experiments/2017-05-14--SimGrid-HPL--loopback-ethernet/$(hostname)_hpl_20000_${cores}_no_turbo_numactl.out 2>&1
#echo "------ NO TURBO, NO numactl ------"
#smpirun -np $cores -platform platform_taurus_hpl.xml -hostfile ~/src/hpl-2.2/bin/Debian/hostnames-144 --log=smpi_bench.thres:critical --cfg=tracing:yes --cfg=tracing/filename:/tmp/tracing --cfg=tracing/smpi:1 --cfg=tracing/smpi/computing:yes --cfg=smpi/privatize-global-variables:yes --cfg=plugin:Energy ./xhpl  >| /home/cheinrich/experiments/2017-05-05--SimGrid-HPL--low-bandwidth/$(hostname)_hpl_20000_${cores}_no_turbo_no_numactl.out 2>&1

done
