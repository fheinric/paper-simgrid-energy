#!/bin/zsh

starttime=$(date +"%s")
test_cores=(1 4 8 12)
echo "timestamp,no_cores_active,set_frequency"
for cores in $test_cores; do
  ~/experiments/setup_cores.zsh $cores > /dev/null 2>&1
  for freq in $(seq 1200 100 2300); do
    sudo cpupower --cpu all frequency-set -d $freq"MHz" -u $freq"MHz" > /dev/null 2>&1
    echo "$(date +"%s"),$cores,$freq"
    sleep $((10*60))
  done
done
