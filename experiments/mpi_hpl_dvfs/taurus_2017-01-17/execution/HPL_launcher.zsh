#!/usr/bin/env zsh
nodes_arr=(12 8 4 1)
for run in {1..1}; do
  for freq in $(seq 2300 -100 1200); do
    for nodes in ${nodes_arr}; do
      echo "Iteration: ${run} with frequency: ${freq}"
      procs=$((12*$nodes))
      mkdir -p /tmp/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off
      mkdir -p /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/
      export TAU_TRACE=0
      taktuk --login 'root' -f =(uniq /home/cheinrich/src/hpl-2.2/bin/Debian/hostnames-144) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d '${freq}'MHz -u '${freq}'MHz' ]
      taktuk -f =(uniq /home/cheinrich/src/hpl-2.2/bin/Debian/hostnames-144) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
      taktuk -f =(uniq /home/cheinrich/src/hpl-2.2/bin/Debian/hostnames-144) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
      taktuk -f =(uniq /home/cheinrich/src/hpl-2.2/bin/Debian/hostnames-144) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/HPL_20000_NODES='${nodes}'_CORES=12_NPROCS='${procs}'_FREQUENCY='${freq}'_TRACING=off/experiment.$(hostname).cpufreq-info' ]
      cd /home/cheinrich/src/hpl-2.2/bin/Debian/
      unlink HPL.dat
      ln -s ./HPL.dat.${procs} HPL.dat
      echo 'mpirun --timestamp-output --report-bindings -bycore -bind-to-core --timestamp-output -np '$procs' --mca orte_rsh_agent ssh --mca pml ob1 --mca btl tcp,self -machinefile ./hostnames-144 ./xhpl > /tmp/HPL_20000_NODES='${nodes}'_CORES=12_NPROCS='${procs}'_FREQUENCY='${freq}'_TRACING=off/experiment.stdout 2> /tmp/HPL_20000_NODES='${nodes}'_CORES=12_NPROCS='${procs}'_FREQUENCY='${freq}'_TRACING=off/experiment.stderr' > /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/experiment.command
      env > /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/experiment.env
      echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/experiment_timings
      mpirun --timestamp-output --report-bindings -bycore -bind-to-core --timestamp-output -np $procs --mca orte_rsh_agent ssh --mca pml ob1 --mca btl tcp,self -machinefile ./hostnames-144 ./xhpl > /tmp/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/experiment.stdout 2> /tmp/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/experiment.stderr > /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/experiment.stdout
      echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/experiment_timings
      echo 'HPL_20000_NODES='${nodes}'_CORES=12_NPROCS='${procs}'_FREQUENCY='${freq}'_TRACING=off/' >>| /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/experiment_timings
      cp /tmp/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/experiment.* /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/
      cp ./HPL.dat /home/cheinrich/experiments/2017-01-17--MPI-HPL-DVFS/HPL_20000_NODES=${nodes}_CORES=12_NPROCS=${procs}_FREQUENCY=${freq}_TRACING=off/
      sleep 10
    done
  done
done
