#!/usr/bin/env zsh
export TAU_MAX_RECORDS=125829120
export TAU_TRACE=1
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2200_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2100_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=2000_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1900_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1800_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1700_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1600_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1500_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1400_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=12_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=12_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=12_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 12' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=144 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 144  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=12_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.144 > /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=12_NPROCS=144_FREQUENCY=1200_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2200_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2100_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2100_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=2000_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=2000_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1900_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1900_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1800_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1800_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1700_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1700_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1600_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1600_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1500_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1500_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1400_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1400_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=8_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=8_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=64 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 64  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.64 > /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=8_NPROCS=64_FREQUENCY=1200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 8' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=96 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 96  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=8_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.96 > /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=8_NPROCS=96_FREQUENCY=1200_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2200_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2100_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2100_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2100_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=2000_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=2000_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=2000_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1900_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1900_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1900_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1800_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1800_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1800_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1700_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1700_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1700_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1600_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1600_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1600_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1500_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1500_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1500_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1400_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1400_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1400_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=4_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=16 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 16  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.16 > /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=4_NPROCS=16_FREQUENCY=1200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=32 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 32  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.32 > /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=4_NPROCS=32_FREQUENCY=1200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 4' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=48 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 48  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=4_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.48 > /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=4_NPROCS=48_FREQUENCY=1200_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2200MHz -u 2200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2200_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2100_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2100_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2100_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2100MHz -u 2100MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2100_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=2000_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=2000_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=2000_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 2000MHz -u 2000MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=2000_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1900_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1900_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1900_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1900MHz -u 1900MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1900_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1800_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1800_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1800_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1800MHz -u 1800MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1800_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1700_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1700_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1700_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1700MHz -u 1700MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1700_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1600_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1600_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1600_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1600MHz -u 1600MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1600_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1500_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1500_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1500_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1500MHz -u 1500MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1500_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1400_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1400_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1400_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1400MHz -u 1400MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1400_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1300_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1300_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1300_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1300MHz -u 1300MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1300_TRACING=off_RUNID=5
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1 ] 
mkdir /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=1 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.compilation.stderr
cd /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 1  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=1 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=1_CORES=1_NPROCS=1_FREQUENCY=1200_TRACING=off_RUNID=1
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3 ] 
mkdir /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=4 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.compilation.stderr
cd /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 4  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=4 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.4 > /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stdout 2> /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=4_CORES=1_NPROCS=4_FREQUENCY=1200_TRACING=off_RUNID=3
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4 ] 
mkdir /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=8 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.compilation.stderr
cd /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 8  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=8 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.8 > /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stdout 2> /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=8_CORES=1_NPROCS=8_FREQUENCY=1200_TRACING=off_RUNID=4
sleep 10
taktuk -c 'ssh' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5 ] 
mkdir /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5
mkdir /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5
export TAU_TRACE=0
taktuk --login 'root' -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d 1200MHz -u 1200MHz' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/setup_cores.zsh 1' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]
taktuk -f =(uniq /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12) broadcast exec [ 'cpufreq-info > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.$(hostname).cpufreq-info' ]
cd /home/cheinrich/src/NPB3.3-MPI
make clean && make EP NPROCS=12 CLASS=C > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stdout 2> /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.compilation.stderr
cd /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5
echo 'mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr' > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.command
env > /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.env
cp /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.machinefile
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
mpirun --report-bindings -bycore -bind-to-core --timestamp-output -np 12  --mca orte_rsh_agent 'ssh' --mca pml ob1 --mca btl tcp,self -machinefile /home/cheinrich/machinefiles/2016-10-14/taurus_LU_CORES=1_NODES=12 /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.12 > /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stdout 2> /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.stderr
echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
echo 'EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5' >>| /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/experiment_timings
cp /tmp/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5/experiment.* /home/cheinrich/experiments/2016-11-19--MPI-EP-DVFS/EP_C_NODES=12_CORES=1_NPROCS=12_FREQUENCY=1200_TRACING=off_RUNID=5
sleep 10
