#!/usr/bin/env zsh
for freq in $(seq 2300 -100 1200); do
  mkdir /tmp/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1
  mkdir -p /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/$(hostname)/
  export TAU_TRACE=0
  sudo cpupower -c all frequency-set -g userspace; sudo cpupower -c all frequency-set -d ${freq}MHz -u ${freq}MHz
  ~/experiments/setup_cores.zsh 12
  ~/experiments/disable_turbomode.zsh
  cpufreq-info > /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/$(hostname)/experiment.$(hostname).cpufreq-info
  cd /home/cheinrich/src/NPB3.3-MPI
  cd /tmp/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1
  echo 'mpirun --timestamp-output --report-bindings -bycore -bind-to-core --timestamp-output -np 1 --mca orte_rsh_agent ssh --mca pml ob1 --mca btl tcp,self -H localhost /home/cheinrich/src/NPB3.3-MPI/bin/lu.C.1 > /tmp/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY='${freq}'_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY='${freq}'_TRACING=off_RUNID=1/experiment.stderr' > /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/$(hostname)/experiment.command
  env > /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/$(hostname)/experiment.env
  echo $(hostname) > /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/$(hostname)/experiment.machinefile
  echo -n "$(date '+%s')," >>| /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/experiment_timings_$(hostname)
  mpirun --timestamp-output --report-bindings -bycore -bind-to-core --timestamp-output -np 1 --mca orte_rsh_agent ssh --mca pml ob1 --mca btl tcp,self -H localhost /home/cheinrich/src/NPB3.3-MPI/bin/lu.C.1 > /tmp/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/experiment.stdout 2> /tmp/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/experiment.stderr
  echo -n "$(date '+%s')," >>| /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/experiment_timings_$(hostname)
  echo 'LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY='${freq}'_TRACING=off_RUNID=1/' >>| /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/experiment_timings_$(hostname)
  cp /tmp/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/experiment.* /home/cheinrich/experiments/2016-11-05--MPI-LU-1-core/LU_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off_RUNID=1/$(hostname)/
done
