#include "mpi.h"
#include <stdio.h>
#include <unistd.h>
#include "time.h"

/*
 * I took some of this code from http://mpi.deino.net/mpi_functions/MPI_Iprobe.html
 */
 
int main( int argc, char * argv[] )
{
    int rank;
    int sendMsg = 123;
    int recvMsg = 0;
    int flag = 0;
    int numprocs;
    int count;
    MPI_Status status;
    MPI_Request request;
    int errs = 0;
 
    MPI_Init( &argc, &argv );
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs); 
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if(rank == 0)
    {
      printf("%lu\n", (unsigned long)time(NULL)); 
      sleep(30);
      for (int dest = 1; dest < numprocs; dest++) {
        MPI_Isend( &sendMsg, 1, MPI_INT, dest, 0, MPI_COMM_WORLD, &request );
      }
      printf("%lu\n", (unsigned long)time(NULL)); 
      MPI_Wait( &request, &status );
    }
    else {
      while(!flag)
      {
          MPI_Iprobe( 0, 0, MPI_COMM_WORLD, &flag, &status );
      }
      MPI_Recv( &recvMsg, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &status );
      if (recvMsg != 123)
      {
          errs++;
      }
    }
 
    MPI_Finalize();
    return errs;
}
