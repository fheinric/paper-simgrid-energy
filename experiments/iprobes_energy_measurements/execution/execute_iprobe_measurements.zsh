#!/usr/bin/zsh

unlink experiment_timings
touch experiment_timings
for freq in $(seq 2300 -100 1200); do
 ssh taurus-8 "sudo cpupower -c all frequency-set -d ${freq}MHz -u ${freq}MHz" >| /dev/null
 for i in {2..13}; do
   paste -d, =(date "+%s.%N") =(echo $freq) =(echo $(($i-1))) >> experiment_timings
   mpirun -np $i --report-bindings -bycore -bind-to-core --mca pml ob1 --mca btl tcp,self -machinefile hostnames ./a.out
 done
done
