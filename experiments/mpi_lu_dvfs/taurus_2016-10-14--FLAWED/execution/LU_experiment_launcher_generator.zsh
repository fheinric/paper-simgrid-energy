#!/usr/bin/zsh

# Setup machines...
SSH_COMMAND="ssh"
CLASS=C
DESIGN_OF_EXPERIMENTS_FILE=$(pwd)/smpi_lu_doe.csv
HOME=/home/cheinrich
MACHINEFILE=""
MACHINES=($(cat $OAR_NODE_FILE | uniq | grep taurus | grep -v "taurus-12" | sort -r))

echo "#!/usr/bin/env zsh"
echo "export TAU_MAX_RECORDS=$((120*1024*1024))" # 120 MB buffer
echo 'export TAU_TRACE=1'

select_machinefile() {
  [[ $# -eq 2 ]] || return

  CORES=$1
  NODES=$2
  MACHINEFILE="$HOME/machinefiles/$(date "+%Y-%m-%d")/taurus_LU_CORES=${CORES}_NODES=${NODES}"

  if [[ ! -e $MACHINEFILE ]]; then
    touch $MACHINEFILE
  else
    return 0
  fi

  for ((i=1; i<=$NODES;i++)); do
    for ((j=1;j<=$CORES;j++)); do
      print $MACHINES[$i] >> $MACHINEFILE
    done
  done
}

STORAGE_PATH="$HOME/experiments/$(date "+%Y-%m-%d")"
awk -F'","|^"|"$|",|,"' 'NR <= 1 {next} {print $2, $3, $4, $5, $6}' $DESIGN_OF_EXPERIMENTS_FILE | while read name CORES NODES TRACING FREQUENCY; do
  NPROCS=$((${NODES}*${CORES}))
  TRACEDIR_NAME="LU_${CLASS}_NODES=${NODES}_CORES=${CORES}_NPROCS=${NPROCS}_FREQUENCY=${FREQUENCY}_TRACING=${TRACING}_RUNID=${name}"
  STORAGE_DIR="$STORAGE_PATH/${TRACEDIR_NAME}"
  TRACEDIR="/tmp/${TRACEDIR_NAME}"
  START_STOP_FILE=${STORAGE_DIR:h}/experiment_timings

  select_machinefile $CORES $NODES

  echo "taktuk -c '${SSH_COMMAND}' -f =(uniq $MACHINEFILE) broadcast exec [ mkdir $TRACEDIR ] "
  echo "mkdir $TRACEDIR" # Job could connect to a host not in $MACHINEFILE; then
                         # this host could not cd into $TRACEDIR and also the
                         # other nodes would write their traces somewhere else...
  echo "mkdir ${STORAGE_DIR}"

  if [[ $TRACING = "on" ]]; then
    echo "export TRACEDIR=$TRACEDIR"
    NPB_DIR="$HOME/src/NPB3.3-MPI-TAU"
    MPI_FLAGS="-x TAU_THROTTLE -x TAU_TRACE -x TRACEDIR -x TAU_MAX_RECORDS"
    echo "export TAU_TRACE=1"
    echo "export TAU_THROTTLE=0"
  else
    NPB_DIR="$HOME/src/NPB3.3-MPI"
    MPI_FLAGS=""
    echo "export TAU_TRACE=0"
  fi
  # Setup CPU frequency ...
  echo "taktuk --login 'root' -f =(uniq $MACHINEFILE) broadcast exec [ 'cpupower -c all frequency-set -g userspace; cpupower -c all frequency-set -d ${FREQUENCY}MHz -u ${FREQUENCY}MHz' ]"
  # Setup cores
  echo "taktuk -f =(uniq $MACHINEFILE) broadcast exec [ '~/experiments/setup_cores.zsh $CORES' ]"
  # Bringing cores back online will enable turbomode again! -> So we disable it...
  echo "taktuk -f =(uniq $MACHINEFILE) broadcast exec [ '~/experiments/disable_turbomode.zsh' ]"
  # Let's log some information, so that we can check later. Just to be safe...
  echo "taktuk -f =(uniq $MACHINEFILE) broadcast exec [ 'cpufreq-info > "${STORAGE_DIR}"/experiment.\$(hostname).cpufreq-info' ]"

  echo "cd $NPB_DIR"
  echo "make clean && make LU NPROCS=${NPROCS} CLASS=${CLASS} > $STORAGE_DIR/experiment.compilation.stdout 2> $STORAGE_DIR/experiment.compilation.stderr"
  echo "cd $TRACEDIR"
  COMMAND="mpirun --timestamp-output -np $NPROCS $MPI_FLAGS --mca plm_rsh_agent '$SSH_COMMAND' --mca pml ob1 --mca btl tcp,self -machinefile $MACHINEFILE $NPB_DIR/bin/lu.${CLASS}.${NPROCS} > ${TRACEDIR}/experiment.stdout 2> $TRACEDIR/experiment.stderr"
  echo "echo '$COMMAND' > $STORAGE_DIR/experiment.command"
  echo "env > $STORAGE_DIR/experiment.env"
  echo "cp $MACHINEFILE $STORAGE_DIR/experiment.machinefile"

  echo "echo -n \"\$(date '+%s'),\" >>| ${START_STOP_FILE}"
  echo "$COMMAND"
  echo "echo -n \"\$(date '+%s'),\" >>| ${START_STOP_FILE}"
  echo "echo '${STORAGE_DIR:t}' >>| ${START_STOP_FILE}"

  # Any output that is produced during the run should be saved locally, not on the NFS.
  # After the run, however, we have to copy it as well...
  echo "cp $TRACEDIR/experiment.* $STORAGE_DIR"

  if [[ $TRACING = "on" ]]; then
    echo "taktuk -c '${SSH_COMMAND}' -f =(uniq $MACHINEFILE) broadcast exec [ mv '$TRACEDIR/*' ${STORAGE_DIR} ] > ${STORAGE_DIR}/experiment.taktuk_mv.output "
  fi

  echo "sleep 10"
done
