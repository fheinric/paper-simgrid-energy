#!/usr/bin/env zsh
for run in {1..10}; do
  for freq in $(seq 2300 -100 1200 | shuf); do
    echo "Iteration: ${run} with frequency: ${freq}"
    mkdir -p /tmp/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off
    mkdir -p /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/$(hostname)/
    export TAU_TRACE=0
    sudo cpupower -c all frequency-set -g userspace; sudo cpupower -c all frequency-set -d ${freq}MHz -u ${freq}MHz
    ~/experiments/setup_cores.zsh 12
    ~/experiments/disable_turbomode.zsh
    cpufreq-info > /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/$(hostname)/experiment.$(hostname).cpufreq-info
    cd /home/cheinrich/src/NPB3.3-MPI
    cd /tmp/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off
    echo 'mpirun --timestamp-output --report-bindings -bycore -bind-to-core --timestamp-output -np 1 --mca orte_rsh_agent ssh --mca pml ob1 --mca btl tcp,self -H localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/iteration_'${run}'/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY='${freq}'_TRACING=off/experiment.stdout 2> /tmp/iteration_'${run}'/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY='${freq}'_TRACING=off/experiment.stderr' > /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/$(hostname)/experiment.command
    env > /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/$(hostname)/experiment.env
    echo $(hostname) > /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/$(hostname)/experiment.machinefile
    echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/experiment_timings_$(hostname)
    mpirun --timestamp-output --report-bindings -bycore -bind-to-core --timestamp-output -np 1 --mca orte_rsh_agent ssh --mca pml ob1 --mca btl tcp,self -H localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost,localhost /home/cheinrich/src/NPB3.3-MPI/bin/ep.C.1 > /tmp/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/experiment.stdout 2> /tmp/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/experiment.stderr
    echo -n "$(date '+%s.%N')," >>| /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/experiment_timings_$(hostname)
    echo 'EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY='${freq}'_TRACING=off/' >>| /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/experiment_timings_$(hostname)
    cp /tmp/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/experiment.* /home/cheinrich/experiments/2016-11-25--MPI-EP-1-core/iteration_${run}/EP_C_NODES=1_CORES=12_NPROCS=1_FREQUENCY=${freq}_TRACING=off/$(hostname)/
    sleep 10
  done
done
