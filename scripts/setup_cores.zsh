#!/bin/zsh

ACTIVE_CORES=$1 # How many cores should we activate?

for cpu in /sys/devices/system/cpu/cpu[0-9]*; do
  sudo zsh -c "echo '0' >| ${cpu}/online"
done

for cpu in {0..$((${ACTIVE_CORES}-1))}; do
  sudo zsh -c "echo '1' >| /sys/devices/system/cpu/cpu${cpu}/online"
done
  
