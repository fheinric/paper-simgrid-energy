#!/bin/zsh

cat $OAR_NODEFILE | uniq >| ~/nodefile

# Enable 12 cores
taktuk -f ~/nodefile broadcast exec [ 'for i in {1..11}; do sudo zsh -c "echo 1 >| /sys/devices/system/cpu/cpu$i/online"; done' ]

# Disable hyperthreading
taktuk -f ~/nodefile broadcast exec [ 'for i in {12..23}; do sudo zsh -c "echo 0 >| /sys/devices/system/cpu/cpu$i/online"; done' ]

# Disable turbomode
taktuk -f ~/nodefile broadcast exec [ 'sudo toggle_turbomode' ]

# Fix frequency
taktuk -f ~/nodefile broadcast exec [ 'sudo cpupower -c all frequency-set -g userspace && sudo cpupower -c all frequency-set -d 2300MHz -u 2300MHz' ]
