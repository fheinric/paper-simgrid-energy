#!/usr/bin/env python

import os, sys, re

import gc

from matplotlib.pyplot import *
from matplotlib.ticker import *
from matplotlib.artist import *
from pylab import *

#xkcd()

plt.style.use('seaborn-deep')

fig, ax = plt.subplots()
tight_layout()
plt.xlabel('CPU utilization (percentage)', size=14, weight='bold')
ax.set_ylabel('Power (Watts)', size=14, weight='bold')
ax.plot([0,1],[1,2], linewidth=3.0, label='$Freq_0$')
ax.plot([0,1],[1,4], linewidth=3.0, label='$Freq_1$')
ax.plot([0,1],[1,8], linewidth=3.0, label='$Freq_i$')
ax.plot([0,1],[1,16], linewidth=3.0, label='$Freq_n$')
ax.axis([0,1,-5,18])
ax.grid(True, color='0.25', linestyle=':', linewidth=2)
ax.legend(loc='upper left', fontsize=14)
#ax.set_xticklabels(())
ax.set_yticks([0.8,2,4,8,16])
ax.set_yticklabels(('$P_{idle}$', '$P_0$', '$P_1$', '$P_i$', '$P_n$'), size=14)
#ax2.set_yticklabels(('$P_{i-1}^{min}$', '', '$P_{i-1}^{max}$'), size=20)
#ax3.set_yticklabels(('$P_i^{min}$', '', '', '$P_i^{max}$'), size=20)
#ax4.set_yticklabels(('$P_n^{min}$', '', '', '', '$P_n^{max}$'), size=20)
#ax.text(0.25, -1, '$Freq_0$', size=20)
#ax2.text(0.20, -1, '$Freq_{i-1}$', size=20)
#ax3.text(0.25, -1, '$Freq_i$', size=20)
#ax4.text(0.25, -1, '$Freq_n$', size=20)
ax.set_xticks([0, 0.2, 0.4, 0.6, 0.8, 1])
ax.set_xticklabels(('0%', '', '', '', '', '100%'), size=14)

#plt.savefig('cost-model-dvfs.png')

fig.set_size_inches(5, 4)
plt.savefig('cost-model-dvfs.pdf', bbox_inches='tight')

show()
