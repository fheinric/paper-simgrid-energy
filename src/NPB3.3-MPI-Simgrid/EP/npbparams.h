c NPROCS = 96 CLASS = C
c  
c  
c  This file is generated automatically by the setparams utility.
c  It sets the number of processors and the class of the NPB
c  in this directory. Do not modify it by hand.
c  
        character class
        parameter (class ='C')
        integer m, npm
        parameter (m=32, npm=96)
        logical  convertdouble
        parameter (convertdouble = .false.)
        character*11 compiletime
        parameter (compiletime='11 Sep 2016')
        character*5 npbversion
        parameter (npbversion='3.3.1')
        character*6 cs1
        parameter (cs1='smpiff')
        character*9 cs2
        parameter (cs2='$(MPIF77)')
        character*41 cs3
        parameter (cs3='-lm -L/home/cheinrich/local/lib -lsimgrid')
        character*46 cs4
        parameter (cs4='-I/home/cheinrich/local/include/smpi -I/hom...')
        character*24 cs5
        parameter (cs5='-O2 -trace-call-location')
        character*3 cs6
        parameter (cs6='-O2')
        character*6 cs7
        parameter (cs7='randi8')
