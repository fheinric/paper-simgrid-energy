#!/bin/zsh

NPROCS=$1
EXPERIMENT_ITERATION="RUN_4"

make LU NPROCS=${NPROCS} CLASS=C
export TAU_TRACE=1
export TRACEDIR=/home/cheinrich/traces/LU_C_NPROCS=${NPROCS}_NODES=12.${EXPERIMENT_ITERATION}

mkdir ${TRACEDIR}

cat $OAR_NODE_FILE > ~/traces/LU_C_NPROCS=${NPROCS}_NODES=12.${EXPERIMENT_ITERATION}.machinefile
mpirun -np ${NPROCS} -x TAU_TRACE -x TRACEDIR --mca plm_rsh_agent "oarsh" --mca pml ob1 --mca btl tcp,self -machinefile $OAR_NODE_FILE $HOME/src/NPB3.3-MPI/bin/lu.C.${NPROCS} > ~/traces/LU_C_NPROCS=${NPROCS}_NODES=12.${EXPERIMENT_ITERATION}.stdout



mv tautrace* ~/traces/LU_C_NPROCS=${NPROCS}_NODES=12.${EXPERIMENT_ITERATION}/
mv events* ~/traces/LU_C_NPROCS=${NPROCS}_NODES=12.${EXPERIMENT_ITERATION}/
