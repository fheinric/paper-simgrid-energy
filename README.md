This repository contains all experiments, scripts and information we've gathered while writing the article "[Predicting the Energy Consumption of MPI~Applications at Scale Using a Single Node](./paper-cluster.pdf)" submitted to the [Cluster 2017](https://cluster17.github.io/) conference.

The main paper has been written in [org-mode](http://www.org-mode.org) and its source can be found in [paper-cluster.org](./paper-cluster.org) whereas
a previous submission with more emphasis on experimental control has been added as a [research report](https://hal.inria.fr/hal-01446134) whose sources are [here](./research_report.org).

Lastly, all figures that we used in the paper were generated via the file [paper-cluster-figures.org](./paper-cluster-figures.org). From this file, you can re-generate the figures,
inspect our R-code etc. To this end, make sure you load the data in that file first, see the headline "Load data from all experiments".